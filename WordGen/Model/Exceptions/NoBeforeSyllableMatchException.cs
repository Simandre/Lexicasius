﻿using System;
using System.Runtime.Serialization;

namespace WordGen.Model.Exceptions {
	/// <summary>
	/// Aucune syllabe adaptée n'as été trouvée. Completez le syllabaire avec d'avantages de syllabes ou changez le comportement de celui-ci lors de l'ajout d'une syllabe.
	/// </summary>
	[Serializable]
	internal class NoBeforeSyllableMatchException : GeneratorException {

		public NoBeforeSyllableMatchException() : base() {
		}

		public NoBeforeSyllableMatchException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary) {
		}

		public NoBeforeSyllableMatchException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message) {
		}

		public NoBeforeSyllableMatchException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message, Exception innerException) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message, innerException) {
		}
	}
}