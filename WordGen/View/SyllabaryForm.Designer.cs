﻿namespace WordGen.View {
	partial class SyllabaryForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SyllabaryForm));
			this.LsyllabaryName = new System.Windows.Forms.Label();
			this.TBsyllabaryName = new System.Windows.Forms.TextBox();
			this.TBconsonants = new System.Windows.Forms.TextBox();
			this.TBvowels = new System.Windows.Forms.TextBox();
			this.TBsyllables = new System.Windows.Forms.TextBox();
			this.Cliste = new System.Windows.Forms.TableLayoutPanel();
			this.Lsyllable = new System.Windows.Forms.Label();
			this.Lconsonant = new System.Windows.Forms.Label();
			this.Lvowel = new System.Windows.Forms.Label();
			this.DDbeginSyllables = new System.Windows.Forms.ComboBox();
			this.DDendSyllables = new System.Windows.Forms.ComboBox();
			this.EPsyllabary = new System.Windows.Forms.ErrorProvider(this.components);
			this.BCancel = new System.Windows.Forms.Button();
			this.BOk = new System.Windows.Forms.Button();
			this.LbeginSyllable = new System.Windows.Forms.Label();
			this.LendSyllable = new System.Windows.Forms.Label();
			this.TTsyllablesHelp = new System.Windows.Forms.ToolTip(this.components);
			this.LhelpBeginSyllable = new System.Windows.Forms.Label();
			this.LhelpEndSyllable = new System.Windows.Forms.Label();
			this.Cliste.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.EPsyllabary)).BeginInit();
			this.SuspendLayout();
			// 
			// LsyllabaryName
			// 
			this.LsyllabaryName.AutoSize = true;
			this.LsyllabaryName.Location = new System.Drawing.Point(12, 15);
			this.LsyllabaryName.Name = "LsyllabaryName";
			this.LsyllabaryName.Size = new System.Drawing.Size(100, 13);
			this.LsyllabaryName.TabIndex = 0;
			this.LsyllabaryName.Text = "Nom du Syllabaire";
			// 
			// TBsyllabaryName
			// 
			this.TBsyllabaryName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TBsyllabaryName.Location = new System.Drawing.Point(118, 12);
			this.TBsyllabaryName.MaxLength = 50;
			this.TBsyllabaryName.Name = "TBsyllabaryName";
			this.TBsyllabaryName.Size = new System.Drawing.Size(244, 22);
			this.TBsyllabaryName.TabIndex = 1;
			this.TBsyllabaryName.TextChanged += new System.EventHandler(this.TBsyllabaryName_TextChanged);
			this.TBsyllabaryName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// TBconsonants
			// 
			this.TBconsonants.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TBconsonants.Location = new System.Drawing.Point(128, 23);
			this.TBconsonants.Multiline = true;
			this.TBconsonants.Name = "TBconsonants";
			this.TBconsonants.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TBconsonants.Size = new System.Drawing.Size(119, 202);
			this.TBconsonants.TabIndex = 4;
			this.TBconsonants.WordWrap = false;
			this.TBconsonants.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// TBvowels
			// 
			this.TBvowels.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TBvowels.Location = new System.Drawing.Point(3, 23);
			this.TBvowels.Multiline = true;
			this.TBvowels.Name = "TBvowels";
			this.TBvowels.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TBvowels.Size = new System.Drawing.Size(119, 202);
			this.TBvowels.TabIndex = 3;
			this.TBvowels.WordWrap = false;
			this.TBvowels.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// TBsyllables
			// 
			this.TBsyllables.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TBsyllables.Location = new System.Drawing.Point(253, 23);
			this.TBsyllables.Multiline = true;
			this.TBsyllables.Name = "TBsyllables";
			this.TBsyllables.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TBsyllables.Size = new System.Drawing.Size(120, 202);
			this.TBsyllables.TabIndex = 5;
			this.TBsyllables.WordWrap = false;
			this.TBsyllables.TextChanged += new System.EventHandler(this.TBsyllables_TextChanged);
			this.TBsyllables.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// Cliste
			// 
			this.Cliste.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Cliste.ColumnCount = 3;
			this.Cliste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
			this.Cliste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.Cliste.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
			this.Cliste.Controls.Add(this.Lsyllable, 2, 0);
			this.Cliste.Controls.Add(this.Lconsonant, 1, 0);
			this.Cliste.Controls.Add(this.Lvowel, 0, 0);
			this.Cliste.Controls.Add(this.TBsyllables, 2, 1);
			this.Cliste.Controls.Add(this.TBvowels, 0, 1);
			this.Cliste.Controls.Add(this.TBconsonants, 1, 1);
			this.Cliste.Location = new System.Drawing.Point(12, 40);
			this.Cliste.Name = "Cliste";
			this.Cliste.RowCount = 2;
			this.Cliste.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.Cliste.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Cliste.Size = new System.Drawing.Size(376, 228);
			this.Cliste.TabIndex = 2;
			// 
			// Lsyllable
			// 
			this.Lsyllable.AutoSize = true;
			this.Lsyllable.Dock = System.Windows.Forms.DockStyle.Left;
			this.Lsyllable.Location = new System.Drawing.Point(253, 0);
			this.Lsyllable.Name = "Lsyllable";
			this.Lsyllable.Size = new System.Drawing.Size(43, 20);
			this.Lsyllable.TabIndex = 2;
			this.Lsyllable.Text = "Syllabe";
			this.Lsyllable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Lconsonant
			// 
			this.Lconsonant.AutoSize = true;
			this.Lconsonant.Dock = System.Windows.Forms.DockStyle.Left;
			this.Lconsonant.Location = new System.Drawing.Point(128, 0);
			this.Lconsonant.Name = "Lconsonant";
			this.Lconsonant.Size = new System.Drawing.Size(60, 20);
			this.Lconsonant.TabIndex = 1;
			this.Lconsonant.Text = "Consonne";
			this.Lconsonant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Lvowel
			// 
			this.Lvowel.AutoSize = true;
			this.Lvowel.Dock = System.Windows.Forms.DockStyle.Left;
			this.Lvowel.Location = new System.Drawing.Point(3, 0);
			this.Lvowel.Name = "Lvowel";
			this.Lvowel.Size = new System.Drawing.Size(43, 20);
			this.Lvowel.TabIndex = 0;
			this.Lvowel.Text = "Voyelle";
			this.Lvowel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DDbeginSyllables
			// 
			this.DDbeginSyllables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DDbeginSyllables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DDbeginSyllables.Enabled = false;
			this.DDbeginSyllables.FormattingEnabled = true;
			this.DDbeginSyllables.Items.AddRange(new object[] {
            "Ne pas adapter",
            "Choisir automatiquement",
            "Intercaller automatiquement"});
			this.DDbeginSyllables.Location = new System.Drawing.Point(196, 277);
			this.DDbeginSyllables.Name = "DDbeginSyllables";
			this.DDbeginSyllables.Size = new System.Drawing.Size(166, 21);
			this.DDbeginSyllables.TabIndex = 4;
			this.DDbeginSyllables.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// DDendSyllables
			// 
			this.DDendSyllables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DDendSyllables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DDendSyllables.Enabled = false;
			this.DDendSyllables.FormattingEnabled = true;
			this.DDendSyllables.Items.AddRange(new object[] {
            "Ne pas adapter",
            "Intercaler automatiquement"});
			this.DDendSyllables.Location = new System.Drawing.Point(196, 304);
			this.DDendSyllables.Name = "DDendSyllables";
			this.DDendSyllables.Size = new System.Drawing.Size(166, 21);
			this.DDendSyllables.TabIndex = 7;
			this.DDendSyllables.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// EPsyllabary
			// 
			this.EPsyllabary.ContainerControl = this;
			this.EPsyllabary.Icon = ((System.Drawing.Icon)(resources.GetObject("EPsyllabary.Icon")));
			// 
			// BCancel
			// 
			this.BCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BCancel.Image = global::WordGen.Properties.Resources.Close_16xLG;
			this.BCancel.Location = new System.Drawing.Point(313, 341);
			this.BCancel.Name = "BCancel";
			this.BCancel.Size = new System.Drawing.Size(75, 23);
			this.BCancel.TabIndex = 10;
			this.BCancel.Text = "&Annuler";
			this.BCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.BCancel.UseVisualStyleBackColor = true;
			this.BCancel.Click += new System.EventHandler(this.BCancel_Click);
			this.BCancel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// BOk
			// 
			this.BOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BOk.Image = global::WordGen.Properties.Resources.StatusAnnotations_Complete_and_ok_16xLG;
			this.BOk.Location = new System.Drawing.Point(232, 341);
			this.BOk.Name = "BOk";
			this.BOk.Size = new System.Drawing.Size(75, 23);
			this.BOk.TabIndex = 9;
			this.BOk.Text = "&OK";
			this.BOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.BOk.UseVisualStyleBackColor = true;
			this.BOk.Click += new System.EventHandler(this.BOk_Click);
			this.BOk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			// 
			// LbeginSyllable
			// 
			this.LbeginSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LbeginSyllable.AutoSize = true;
			this.LbeginSyllable.Location = new System.Drawing.Point(12, 280);
			this.LbeginSyllable.Name = "LbeginSyllable";
			this.LbeginSyllable.Size = new System.Drawing.Size(97, 13);
			this.LbeginSyllable.TabIndex = 3;
			this.LbeginSyllable.Text = "Avant une syllabe";
			// 
			// LendSyllable
			// 
			this.LendSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LendSyllable.AutoSize = true;
			this.LendSyllable.Location = new System.Drawing.Point(12, 307);
			this.LendSyllable.Name = "LendSyllable";
			this.LendSyllable.Size = new System.Drawing.Size(97, 13);
			this.LendSyllable.TabIndex = 6;
			this.LendSyllable.Text = "Après une syllabe";
			// 
			// TTsyllablesHelp
			// 
			this.TTsyllablesHelp.AutomaticDelay = 0;
			this.TTsyllablesHelp.AutoPopDelay = 50000;
			this.TTsyllablesHelp.InitialDelay = 500;
			this.TTsyllablesHelp.ReshowDelay = 100;
			this.TTsyllablesHelp.ShowAlways = true;
			this.TTsyllablesHelp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			// 
			// LhelpBeginSyllable
			// 
			this.LhelpBeginSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.LhelpBeginSyllable.Image = global::WordGen.Properties.Resources.StatusAnnotations_Help_and_inconclusive_16xLG_color;
			this.LhelpBeginSyllable.Location = new System.Drawing.Point(368, 280);
			this.LhelpBeginSyllable.Name = "LhelpBeginSyllable";
			this.LhelpBeginSyllable.Size = new System.Drawing.Size(16, 16);
			this.LhelpBeginSyllable.TabIndex = 5;
			this.LhelpBeginSyllable.MouseHover += new System.EventHandler(this.LhelpBeginSyllable_MouseHover);
			// 
			// LhelpEndSyllable
			// 
			this.LhelpEndSyllable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.LhelpEndSyllable.Image = global::WordGen.Properties.Resources.StatusAnnotations_Help_and_inconclusive_16xLG_color;
			this.LhelpEndSyllable.Location = new System.Drawing.Point(368, 307);
			this.LhelpEndSyllable.Name = "LhelpEndSyllable";
			this.LhelpEndSyllable.Size = new System.Drawing.Size(16, 16);
			this.LhelpEndSyllable.TabIndex = 8;
			this.LhelpEndSyllable.MouseHover += new System.EventHandler(this.LhelpEndSyllable_MouseHover);
			// 
			// SyllabaryForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.BCancel;
			this.ClientSize = new System.Drawing.Size(400, 376);
			this.Controls.Add(this.DDbeginSyllables);
			this.Controls.Add(this.LbeginSyllable);
			this.Controls.Add(this.LhelpBeginSyllable);
			this.Controls.Add(this.DDendSyllables);
			this.Controls.Add(this.LendSyllable);
			this.Controls.Add(this.LhelpEndSyllable);
			this.Controls.Add(this.BCancel);
			this.Controls.Add(this.BOk);
			this.Controls.Add(this.Cliste);
			this.Controls.Add(this.TBsyllabaryName);
			this.Controls.Add(this.LsyllabaryName);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(400, 400);
			this.Name = "SyllabaryForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Syllabaire";
			this.Load += new System.EventHandler(this.Syllabaire_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SyllabaireForm_KeyDown);
			this.Cliste.ResumeLayout(false);
			this.Cliste.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.EPsyllabary)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LsyllabaryName;
		private System.Windows.Forms.TextBox TBsyllabaryName;
		private System.Windows.Forms.TextBox TBconsonants;
		private System.Windows.Forms.TextBox TBvowels;
		private System.Windows.Forms.TextBox TBsyllables;
		private System.Windows.Forms.TableLayoutPanel Cliste;
		private System.Windows.Forms.Button BOk;
		private System.Windows.Forms.Button BCancel;
		private System.Windows.Forms.ComboBox DDbeginSyllables;
		private System.Windows.Forms.ComboBox DDendSyllables;
		private System.Windows.Forms.ErrorProvider EPsyllabary;
		private System.Windows.Forms.ToolTip TTsyllablesHelp;
		private System.Windows.Forms.Label Lsyllable;
		private System.Windows.Forms.Label Lconsonant;
		private System.Windows.Forms.Label Lvowel;
		private System.Windows.Forms.Label LendSyllable;
		private System.Windows.Forms.Label LbeginSyllable;
		private System.Windows.Forms.Label LhelpBeginSyllable;
		private System.Windows.Forms.Label LhelpEndSyllable;

		public object getSyllabaire { get; internal set; }

	}
}