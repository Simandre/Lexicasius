﻿using WordGen.Model;
using WordGen.View.LocalizationString;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;

namespace WordGen.View {
	public partial class ManageSyllabaryForm : Form {
		public ManageSyllabaryForm() {
			InitializeComponent();
			reloadListView();

		}

		private void reloadListView() {
			LVSyllabary.Clear();
			foreach (Syllabary aSillabary in SyllabaryWrapper.GetAll()) {
				// add a ListViewItem with the Sillabary title and the 0th Icon index (specified in the ListView) in the ListView.
				LVSyllabary.Items.Add(aSillabary.title, 0);
			}
		}

		private void Bclose_Click(object sender, EventArgs e) {
			this.Close();
			this.Dispose();
		}

		private void LVsyllabary_SelectedIndexChanged(object sender, EventArgs e) {
			if (LVSyllabary.SelectedItems.Count == 0) {
				Bedit.Enabled = false;
				Bdelete.Enabled = false;
				Bduplicate.Enabled = false;
				Bexport.Enabled = false;
			} else if (LVSyllabary.SelectedItems.Count == 1) {
				Bedit.Enabled = true;
				Bdelete.Enabled = true;
				Bduplicate.Enabled = true;
				Bexport.Enabled = true;
			} else {
				Bedit.Enabled = false;
				Bdelete.Enabled = true;
				Bduplicate.Enabled = true;
				Bexport.Enabled = false;
			}
		}

		// Delete sullabary
		private void Bdelete_Click(object sender, EventArgs e) {
			LVSyllabary_DeleteSelectedItems();
		}


		private void LVSyllabary_KeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Delete) {
				LVSyllabary_DeleteSelectedItems();
			}
		}


		private void LVSyllabary_DeleteSelectedItems() {

			if (LVSyllabary.SelectedItems.Count == 0) {
				return;

			} else if (LVSyllabary.SelectedItems.Count == 1) {
				// 1 item selected.
				// We show a confirmation message box.
				var result = MessageBox.Show(string.Format(Messages.ManageSyllabaireForm_ConfirmDelete1Item, LVSyllabary.SelectedItems[0].Text), Messages.ManageSyllabaireForm_ConfirmDelete1ItemTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				// If no, the deletion exits
				if (result == DialogResult.No)
					return;

			} else {
				// Multiple items selected.
				var result = MessageBox.Show(string.Format(Messages.ManageSyllabaireForm_ConfirmDeleteManyItems, LVSyllabary.SelectedItems.Count), Messages.ManageSyllabaireForm_ConfirmDeleteManyItemsTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				// If no, the deletion exits
				if (result == DialogResult.No)
					return;
			}

			// Deletion
			foreach (ListViewItem aSyllabary in LVSyllabary.SelectedItems) {
				SyllabaryWrapper.Delete(aSyllabary.Text);
			}
			reloadListView();
		}

		private void LVSyllabary_DoubleClick(object sender, EventArgs e) {
			if (LVSyllabary.SelectedItems.Count == 1) {
				// Edit item
				LVSullabary_EditItem();
			} else if (LVSyllabary.SelectedItems.Count == 0) {
				// New item
				LVSyllabary_NewItem();
			}
		}

		private void Bnew_Click(object sender, EventArgs e) {
			LVSyllabary_NewItem();
		}

		private void LVSyllabary_NewItem() {
			SyllabaryForm mySF = new SyllabaryForm();
			if (mySF.ShowDialog() == DialogResult.OK) {
				SyllabaryWrapper.Add(mySF.MySyllabaire);
				reloadListView();
			}
		}


		// Edit sullabary
		private void Bedit_Click(object sender, EventArgs e) {
			LVSullabary_EditItem();
		}

		private void LVSullabary_EditItem() {
			Syllabary thisSyllabary = SyllabaryWrapper.Get(LVSyllabary.SelectedItems[0].Text);
			SyllabaryForm mySF = new SyllabaryForm(thisSyllabary);
			if (mySF.ShowDialog() == DialogResult.OK) {
				SyllabaryWrapper.Replace(thisSyllabary.title, mySF.MySyllabaire);
				reloadListView();
			}
		}

		private void Bduplicate_Click(object sender, EventArgs e) {
			foreach (ListViewItem lvItem in LVSyllabary.SelectedItems) {
				// clone
				Syllabary thisSyllabary = SyllabaryWrapper.Get(lvItem.Text);
				Syllabary clonedSyllabary = (Syllabary)thisSyllabary.Clone();
				SyllabaryWrapper.Add(clonedSyllabary);
			}
			// add on the list
			reloadListView();

		}

		private void Bexport_Click(object sender, EventArgs e) {
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.FileName = LVSyllabary.SelectedItems[0].Text;
			sfd.Filter = "eXtensible Markup Language (*.xml)|*.xml";
			if (sfd.ShowDialog() == DialogResult.OK) {
				SyllabaryIoManager Siom = new SyllabaryIoManager();
				Syllabary thisSyllabary = SyllabaryWrapper.Get(LVSyllabary.SelectedItems[0].Text);
				Siom.saveSyllabary(thisSyllabary, sfd.FileName);
			}

		}

		private void BexportAll_Click(object sender, EventArgs e) {
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			if (fbd.ShowDialog() == DialogResult.OK) {
				SyllabaryIoManager Siom = new SyllabaryIoManager();
				foreach (Syllabary item in SyllabaryWrapper.GetAll()) {
					Siom.saveSyllabary(item, Path.Combine(fbd.SelectedPath, item.title));
				}
			}
		}

		private void Bimport_Click(object sender, EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Multiselect = true;
			ofd.Filter = "eXtensible Markup Language (*.xml)|*.xml";
			if (ofd.ShowDialog() == DialogResult.OK) {
				SyllabaryIoManager Siom = new SyllabaryIoManager();
				foreach (string item in ofd.FileNames) {
					Syllabary thisSyllabary = Siom.loadSyllabary(item);
					SyllabaryWrapper.Add(thisSyllabary);
				}
				reloadListView();
			}
		}

		private void ManageSyllabaryForm_Load(object sender, EventArgs e) {

		}
	}
}
