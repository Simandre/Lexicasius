﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WordGen.Model;
using WordGen.Model.Exceptions;
using WordGen.View;
using WordGen.View.LocalizationString;

namespace WordGen.View {
	public partial class MainForm : Form {

		private ResultForm _rf;

		public MainForm() {
			InitializeComponent();
			reloadDD();
		}

		private void reloadDD() {
			DDsyllbary.Items.Clear();
			foreach (Syllabary item in SyllabaryWrapper.GetAll()) {
				DDsyllbary.Items.Add(item.title);
			}
		}

		private bool validateForm() {
			var everythingOK = true;
			if (SyllabaryWrapper.count() == 0) {
				everythingOK = false;
				EPmain.SetError(DDsyllbary, Messages.MainForm_ErrorNoSyllabaryAvailable);
			}
			if (DDsyllbary.SelectedItem == null) {
				everythingOK = false;
				EPmain.SetError(DDsyllbary, Messages.MainForm_ErrorNoSyllabarySelected);
			}

			if (RBwordsLenght.Checked) {
				// nombre de caractères forcément positif
				if (NUPwordsLenght.Value < 1) {
					everythingOK = false;
					EPmain.SetError(LnbChars, Messages.MainForm_ErrorInvalideWordLenght);
				}
				// nombre de voyelle forcément en %.
				if (NUPvowelPercent.Value > 100 || NUPvowelPercent.Value < 0) {
					everythingOK = false;
					EPmain.SetError(NUPvowelPercent, Messages.MainForm_ErrorInvalidBeginVowelPercent);
				}
				// nombre de syllabe forcément en %.
				if (CBincludeSyllab.Checked && (NUPsyllablePercent.Value > 100 || NUPsyllablePercent.Value < 0)) {
					everythingOK = false;
					EPmain.SetError(NUPsyllablePercent, Messages.MainForm_ErrorInvalidSyllablePercent);
				}

			}

			if (RBcustom.Checked) {
				if (String.IsNullOrEmpty(TBwordStructure.Text.Trim())) {
					everythingOK = false;
					EPmain.SetError(TBwordStructure, Messages.MainForm_ErrorEmptySyntaxe);
				}
				// others ifs

			}

			if (NUPnbWords.Value <= 0) {
				everythingOK = false;
				EPmain.SetError(NUPnbWords, Messages.MainForm_ErrorInvalideNbWords);
			}
			return everythingOK;
		}

		private void BchooseSyllabaire_Click(object sender, EventArgs e) {
			ManageSyllabaryForm mySyllabManager = new ManageSyllabaryForm();
			mySyllabManager.ShowDialog();
			reloadDD();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
			SyllabaryWrapper.SaveToDrive();
			Properties.Settings.Default.PreviousLocation = this.Location;
			Properties.Settings.Default.Save();
		}

		private void Form1_Load(object sender, EventArgs e) {
			// Screen Location
			var nullPoint = new System.Drawing.Point(0, 0);
			if (Properties.Settings.Default.PreviousLocation != null && Properties.Settings.Default.PreviousLocation != nullPoint) {
				this.Location = Properties.Settings.Default.PreviousLocation;
			}

			// Tooltip setup
			TThelp.ToolTipTitle = "Aide";
			TThelp.SetToolTip(LsyntaxHelp, Messages.MainForm_HelpCustomSyntax);
			TThelp.SetToolTip(LvowelPercent, Messages.MainForm_HelpBeginVowelPercent);
			TThelp.SetToolTip(LsyllablePercent, Messages.MainForm_HelpSyllablePercent);
			TThelp.SetToolTip(LbeginSyllablePercent, Messages.MainForm_HelpBeginSyllabaryPercent);

			NUPwordsLenght_ValueChanged(sender, e);
		}

		private void NUPwordsLenght_ValueChanged(object sender, EventArgs e) {
			if (NUPwordsLenght.Value <= 1) {
				LnbChars.Text = string.Format(Messages.MainForm_LabelwordLength, Messages.MainForm_LabelCharSingular);
			} else {
				LnbChars.Text = string.Format(Messages.MainForm_LabelwordLength, Messages.MainForm_LabelCharPlural);
			}
			EPmain.SetError(LnbChars, string.Empty);
		}

		private void BGenerate_Click(object sender, EventArgs e) {
			if (!validateForm()) {
				return;
			}

			if (_rf == null || (_rf != null && !_rf.Visible)) {
				_rf = null;
				_rf = new ResultForm();
				_rf.Show();
				_rf.Location = new System.Drawing.Point(this.Location.X + this.Size.Width, this.Location.Y);
				_rf.isLocked = true;
			}

			var selectedSyllab = SyllabaryWrapper.Get((string)DDsyllbary.SelectedItem);
			var quantity = NUPnbWords.Value;

			Generator gen;
			

			if (RBwordsLenght.Checked) {
				var myGenSett = new GeneratorSettings();
				//myGenSett._syllableBeginProba = NUPbeginSyllablePercent.Value;
				myGenSett._syllableProba = NUPsyllablePercent.Value;
				myGenSett._vowelBeginProba = NUPvowelPercent.Value;
				gen = new Generator(selectedSyllab, Convert.ToUInt32(NUPwordsLenght.Value), myGenSett );
			} else {
				gen = new Generator(selectedSyllab, TBwordStructure.Text);
			}

			try {
				Dictionary<string, GeneratorException> results = gen.generateWords(quantity);
				_rf.setList(results);
			} catch (Exception ex) {
				_rf.Close();
				_rf = null;
				MessageBox.Show(ex.Message, "Erreur : " + ex.GetType(), MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		private void MainForm_Move(object sender, EventArgs e) {
			if (_rf != null && _rf.isLocked) {
				_rf.Location = new System.Drawing.Point(this.Location.X + this.Size.Width, this.Location.Y);
			}
		}

		private void MainForm_Resize(object sender, EventArgs e) {
			if (_rf != null && _rf.isLocked) {
				_rf.Location = new System.Drawing.Point(this.Location.X + this.Size.Width, this.Location.Y);
			}
		}

		private void RBcustom_CheckedChanged(object sender, EventArgs e) {
			TBwordStructure.Enabled = RBcustom.Checked;
			LsyntaxHelp.Enabled = RBcustom.Checked;
			LquickGuide.Enabled = RBcustom.Checked;
		}

		private void RBwordsLenght_CheckedChanged(object sender, EventArgs e) {
			CBincludeSyllab.Enabled = RBwordsLenght.Checked;
			NUPwordsLenght.Enabled = RBwordsLenght.Checked;
			LnbChars.Enabled = RBwordsLenght.Checked;
		}

		private void DDSyllabaire_SelectedIndexChanged(object sender, EventArgs e) {
			EPmain.SetError(DDsyllbary, String.Empty);
		}

		private void TBwordStructure_TextChanged(object sender, EventArgs e) {
			EPmain.SetError(TBwordStructure, string.Empty);
		}

		private void NUPNbWords_ValueChanged(object sender, EventArgs e) {
			EPmain.SetError(NUPnbWords, string.Empty);
		}

		private void CBincludeSyllab_CheckedChanged(object sender, EventArgs e) {
			LsyllablePercent.Enabled = CBincludeSyllab.Checked;
			NUPsyllablePercent.Enabled = CBincludeSyllab.Checked;
			CBallowBeginSyllable.Enabled = CBincludeSyllab.Checked;
			LbeginSyllablePercent.Enabled = (CBincludeSyllab.Checked && CBallowBeginSyllable.Checked);
			NUPbeginSyllablePercent.Enabled = (CBincludeSyllab.Checked && CBallowBeginSyllable.Checked);
		}

		private void CBallowBeginSyllable_CheckedChanged(object sender, EventArgs e) {
			LbeginSyllablePercent.Enabled = (CBincludeSyllab.Checked && CBallowBeginSyllable.Checked);
			NUPbeginSyllablePercent.Enabled = (CBincludeSyllab.Checked && CBallowBeginSyllable.Checked);
		}
	}
}
